#!/bin/bash
PDBID=1D7M
mkdir ${PDBID}
cd ${PDBID}
wget https://files.rcsb.org/download/${PDBID}.pdb
grep -v HETATM ${PDBID}.pdb > 1_protein_tmp.pdb
grep -v CONECT 1_protein_tmp.pdb > 1_protein.pdb
gmx pdb2gmx -f 1_protein.pdb -o 2_processed.gro -p topol.top -ff charmm27 -water tip3p
gmx editconf -f 2_processed.gro -o 3_newbox.gro -c -box 30 3.5 3.5 -princ <<< $'1\n'
gmx solvate -cp 3_newbox.gro -o 4_solv.gro -p topol.top
touch 5_ions.mdp
gmx grompp -f 5_ions.mdp -c 4_solv.gro -p topol.top -o 5_ions.tpr
gmx genion -s 5_ions.tpr -o 5_solv_ions.gro -conc 0.15 -p topol.top -pname NA -nname CL -neutral <<< $'13\n'
cp ../input/*.mdp .
gmx grompp -f 6_em.mdp -c 5_solv_ions.gro -o 6_em.tpr
gmx mdrun -v -deffnm 6_em
gmx grompp -f 7_nvt.mdp -c 6_em.gro -r 6_em.gro -o 7_nvt.tpr
gmx mdrun -v -deffnm 7_nvt
gmx grompp -f 8_npt.mdp -c 7_nvt.gro -r 7_nvt.gro -o 8_npt.tpr
gmx mdrun -v -deffnm 8_npt
cp ../input/9_pull.ndx .
gmx grompp -f 9_pull.mdp -c 8_npt.gro -o 9_pull.tpr -n 9_pull.ndx
gmx mdrun -v -deffnm 9_pull
